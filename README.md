# spacedao-webapp
The SpaceDAO is best accessed through well-designed web apps.

To install the state management pattern + library [vuex4](https://vuex.vuejs.org/installation.html) for Vue3
```
npm install --safe vuex@next
npm install axios vue-axios

```

## Designed by Parametry with Love
- Ionic Framework
- Vue.js

