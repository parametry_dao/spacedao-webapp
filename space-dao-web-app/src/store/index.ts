import { createStore } from "vuex";

const store = createStore({
    state() {
        return {
            space_assets: [
            {
                id: "sa1",
                name: "Satellite 1",
                description: "Communication satellite",
                image: "https://upload.wikimedia.org/wikipedia/commons/3/32/Hubble_01.jpg",
            },
            {
                id: "sa2",
                name: "Satellite 2",
                description: "GEO satellite",
                image: "https://upload.wikimedia.org/wikipedia/commons/3/32/Hubble_01.jpg",
            },
            {
                id: "sa3",
                name: "Station 1",
                description: "Space station",
                image: "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/The_station_pictured_from_the_SpaceX_Crew_Dragon_5_%28cropped%29.jpg/1920px-The_station_pictured_from_the_SpaceX_Crew_Dragon_5_%28cropped%29.jpg",
            },

            ]
        };
    },
    getters: {
        space_assets(state) {
            return state.space_assets;
        },
        space_asset(state) {
            return (assetID: string) => {
                return state.space_assets.find((space_asset) => space_asset.id == assetID);
            };
        },
    },
});

export default store;