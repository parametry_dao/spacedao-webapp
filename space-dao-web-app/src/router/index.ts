import { createRouter, createWebHistory } from '@ionic/vue-router';
import { RouteRecordRaw } from 'vue-router';
import LoginView from '../views/LoginView.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    component: LoginView,
  },
  {
    path: '/space-overview',
    component: () => import('../views/SpaceOverviewView.vue'),
  },
  {
    path: '/user-assets',
    component: () => import('../views/UserAssetsView.vue'),
  },
  {
    path: '/user-assets/:id',
    component: () => import('../views/AssetDetailsView.vue')
  },
  {
    path: '/user-assets/register',
    component: () => import('../views/RegisterAssetView.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

export default router;
